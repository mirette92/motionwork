import { Component, OnInit, Input } from '@angular/core';
import {ServicesService} from '../services.service';
import {TranslateService} from '@ngx-translate/core';
@Component({
  selector: 'app-services',
  templateUrl: './services.component.html',
  styleUrls: ['./services.component.css']
})
export class ServicesComponent implements OnInit {

  constructor(private _servicesService:ServicesService,private translate: TranslateService) { }
  @Input() arrServices;
  // public arrServices ;
  ngOnInit() {
    
    this._servicesService.listService().subscribe(
      (data:Response)=>{
          if(data['result']['code'] == 200){
            this.arrServices = data['data'];
          }
      }
      );
  }

}
