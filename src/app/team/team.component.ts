import { Component, OnInit,Input } from '@angular/core';
import {ServicesService} from '../services.service';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-team',
  templateUrl: './team.component.html',
  styleUrls: ['./team.component.css']
})
export class TeamComponent implements OnInit {

  constructor(private _servicesService:ServicesService,private translate: TranslateService) { }
  @Input() arrTeam;
  @Input() lang;
  ngOnInit() {
    this._servicesService.listTeam().subscribe(
      (data:Response)=>{
          if(data['result']['code'] == 200){
            this.arrTeam = data['data'];
          }
      }
      );
  }

}
