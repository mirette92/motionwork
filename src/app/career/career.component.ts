import { Component, OnInit ,Input} from '@angular/core';
import {ServicesService} from '../services.service';
import {Router} from '@angular/router';
@Component({
  selector: 'app-career',
  templateUrl: './career.component.html',
  styleUrls: ['./career.component.css']
})
export class CareerComponent implements OnInit {
  @Input() arrCareer;
  @Input() lang;
  constructor(private _servicesService:ServicesService, private router: Router) { }

  ngOnInit() {
    this._servicesService.listCareer().subscribe(
      (data:Response)=>{
          if(data['result']['code'] == 200){
            this.arrCareer = data['data'];
          }
      }
      );
  }
  getCareerDetails(id){
    this.router.navigate(['career/careerDetails', id]);
  }

}
