import { Component, OnInit ,Input } from '@angular/core';
import {ServicesService} from '../../services.service';
import {ActivatedRoute} from '@angular/router';
@Component({
  selector: 'app-career-details',
  templateUrl: './career-details.component.html',
  styleUrls: ['./career-details.component.css']
})
export class CareerDetailsComponent implements OnInit {
  @Input() lang;
  public career_id ; 
  public career;
  constructor(private _servicesService:ServicesService, private route :ActivatedRoute) { }

  ngOnInit() {
    let id = parseInt(this.route.snapshot.paramMap.get('id'));
    this.career_id = id;
    
    this._servicesService.getCareerById(this.career_id).subscribe(
      (data:Response)=>{
        if(data['result']['code'] == 200){
          this.career = data['data'][0];
        }
      });
  }
  
}
