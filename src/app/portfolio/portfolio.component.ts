import { Component, OnInit,Input } from '@angular/core';
import {ServicesService} from '../services.service';
import {Router} from '@angular/router';
@Component({
  selector: 'app-portfolio',
  templateUrl: './portfolio.component.html',
  styleUrls: ['./portfolio.component.css']
})
export class PortfolioComponent implements OnInit {

  constructor(private _servicesService:ServicesService, private router: Router) { }
  @Input() arrSection ;
  public index = 0;
  public arrProject;
  public arrProjectLoop:any = [];
  ngOnInit() {
    this._servicesService.listSection().subscribe(
      (data:Response)=>{
        if(data['result']['code']==200){
          this.arrSection = data['data'];
          this.arrSection.map((obj,index) =>{
            if(obj.project.length == 0){
              this.arrSection.splice(index,1);
            }
          })
          this.getSection(0,this.arrSection);
        }
      });
      
  }
  getProjectDetails(id){
    this.router.navigate(['/project', id]);
  }
  getSection(i,arrSection){
    this.index = i;
    this.arrProject = this.arrSection[i].project

  }
  getAllProject(){
    this.arrProject = [];
    this.arrProjectLoop = [];
    this.arrSection.map((obj) =>{
      // var arrProject = [] 
      this.arrProjectLoop.push(obj.project[0]);
    })
    this.arrProject = this.arrProjectLoop;
  }
}
