import { Component, OnInit,Input } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {ServicesService} from '../services.service';
import * as $ from 'jquery';
import 'bootstrap';
@Component({
  selector: 'app-project-details',
  templateUrl: './project-details.component.html',
  styleUrls: ['./project-details.component.css']
})
export class ProjectDetailsComponent implements OnInit {
  @Input() lang;
  @Input() project;
  public project_id;
  public right = "right";
  public left = "left";
  public isRigt = false;
  public alignClass ={
    "alignLeft": !this.isRigt,
    "alignRight": this.isRigt
  }
  constructor(private route :ActivatedRoute,private _servicesService:ServicesService) { }
  
  ngOnInit() {
    let id = parseInt(this.route.snapshot.paramMap.get('id'));
    this.project_id = id;
    this._servicesService.getProjectById(this.project_id).subscribe(
      (data:Response)=>{
        this.project = data['data'][0];
      }
    );
  }
  ngAfterViewInit() {
    $('.carousel').carousel();
  }

}
