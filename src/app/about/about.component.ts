import { Component, OnInit,Input } from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {ServicesService} from '../services.service';
@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.css']
})
export class AboutComponent implements OnInit {
  @Input() arrAbout;
  constructor(private _servicesService:ServicesService,private translate: TranslateService) { }

  ngOnInit() {
    this._servicesService.listAbout().subscribe(
      (data:Response)=>{
          if(data['result']['code'] == 200){
            this.arrAbout = data['data'];
          }
      }
      );
  }

}
