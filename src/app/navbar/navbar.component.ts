import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {ServicesService} from '../services.service';



@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  @Output() langEmitter:EventEmitter<any> = new EventEmitter();
  @Output() arrServiceEmitter:EventEmitter<any> = new EventEmitter();
  @Output() arrSectionEmitter:EventEmitter<any> = new EventEmitter();
  @Output() arrAboutEmitter:EventEmitter<any> = new EventEmitter();
  @Output() arrBannerEmitter:EventEmitter<any> = new EventEmitter();
  public lang;
  public langC = 'en' ;
  public arrSection;
  constructor(private translate: TranslateService,private _servicesService:ServicesService) { }
  

  ngOnInit() {
  
    if(sessionStorage.getItem('lang') !== null && sessionStorage.getItem('lang') !== "undefined"){

      this.translate.setDefaultLang(JSON.parse(sessionStorage.getItem('lang')));
      this.lang = JSON.parse(sessionStorage.getItem('lang'));
      this.langEmitter.emit(this.lang);
    
    }
    else{
        this.translate.setDefaultLang('en');
        this.lang = 'en';
        //this.lang = JSON.parse(sessionStorage.getItem('lang'));
        sessionStorage.setItem('lang',JSON.stringify(this.lang));
        this.langEmitter.emit(this.lang);
      
        //this.useLanguage(this.lang);

    }

      
 
    
  }
  changeLang(v){
    // this.langC = event.target.value;
    this.translate.setDefaultLang(v);
    this.lang = v;
    //this.lang = JSON.parse(sessionStorage.getItem('lang'));
    sessionStorage.setItem('lang',JSON.stringify(this.lang));
    this.langEmitter.emit(this.lang);
    this._servicesService.listService().subscribe(
      (data:Response)=>{
          if(data['result']['code'] == 200){
            this.arrServiceEmitter.emit(data['data']);
            
          }
      }
      );
      this._servicesService.listAbout().subscribe(
        (data:Response)=>{
            if(data['result']['code'] == 200){
              this.arrAboutEmitter.emit(data['data']);
              
            }
        }
        );
      this._servicesService.listSection().subscribe(
        (data:Response)=>{
          if(data['result']['code']==200){
            this.arrSection = data['data'];
            this.arrSection.map((obj,index) =>{
              if(obj.project.length == 0){
                this.arrSection.splice(index,1);
              }
            this.arrSectionEmitter.emit(data['data']);
            })
          }
        });

        this._servicesService.listBanner().subscribe(
          (data:Response)=>{
              if(data['result']['code'] == 200){
               
                this.arrBannerEmitter.emit(data['data']);
              }
          }
          );  
        
        
  }

}
