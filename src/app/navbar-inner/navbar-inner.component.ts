import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {ServicesService} from '../services.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-navbar-inner',
  templateUrl: './navbar-inner.component.html',
  styleUrls: ['./navbar-inner.component.css']
})
export class NavbarInnerComponent implements OnInit {
  @Output() langEmitter:EventEmitter<any> = new EventEmitter();
  @Output() arrCareerEmitter:EventEmitter<any> = new EventEmitter();
  @Output() objCareerEmitter:EventEmitter<any> = new EventEmitter();
  @Output() arrTeamEmitter:EventEmitter<any> = new EventEmitter();
  @Output() objProjectEmitter:EventEmitter<any> = new EventEmitter();
  @Output() objTermEmitter:EventEmitter<any> = new EventEmitter();
  @Output() objPrivacyEmitter:EventEmitter<any> = new EventEmitter();

  public lang;
  public langC = 'en' ;
  constructor(private translate: TranslateService,private _servicesService:ServicesService, private route :ActivatedRoute) { }
  

  ngOnInit() {
  
    if(sessionStorage.getItem('lang') !== null && sessionStorage.getItem('lang') !== "undefined"){

      this.translate.setDefaultLang(JSON.parse(sessionStorage.getItem('lang')));
      this.lang = JSON.parse(sessionStorage.getItem('lang'));
      this.langEmitter.emit(this.lang);
    
    }
    else{
        this.translate.setDefaultLang('en');
        this.lang = 'en';
        //this.lang = JSON.parse(sessionStorage.getItem('lang'));
        sessionStorage.setItem('lang',JSON.stringify(this.lang));
        this.langEmitter.emit(this.lang);
      
        //this.useLanguage(this.lang);

    }

      
 
    
  }
  changeLang(v){
    // this.langC = event.target.value;
    this.translate.setDefaultLang(v);
    this.lang = v;
    //this.lang = JSON.parse(sessionStorage.getItem('lang'));
    sessionStorage.setItem('lang',JSON.stringify(this.lang));
    this.langEmitter.emit(this.lang);
    this._servicesService.listCareer().subscribe(
      (data:Response)=>{
          if(data['result']['code'] == 200){
            this.arrCareerEmitter.emit(data['data']);
            
          }
      }
      );
      this._servicesService.listTeam().subscribe(
        (data:Response)=>{
            if(data['result']['code'] == 200){
              this.arrTeamEmitter.emit(data['data']);
            }
        }
        );
      let id = parseInt(this.route.snapshot.paramMap.get('id'));
      if(id != null){
        this._servicesService.getCareerById(id).subscribe(
          (data:Response)=>{
            if(data['result']['code'] == 200){
              this.objCareerEmitter.emit(data['data'][0]);
              
            }
          });
      }
      let project_id = parseInt(this.route.snapshot.paramMap.get('id'));
      if(project_id != null){
      
      this._servicesService.getProjectById(project_id).subscribe(
        (data:Response)=>{
          this.objProjectEmitter.emit(data['data'][0]);
          
        }
      );
      }

      this._servicesService.listTerm().subscribe(
        (data:Response)=>{
          this.objTermEmitter.emit(data['data'][0]);
          
        }
      );
      this._servicesService.listPrivacy().subscribe(
        (data:Response)=>{
          this.objPrivacyEmitter.emit(data['data'][0]);
          
        }
      );
  }

}
