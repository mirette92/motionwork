import { Component, OnInit ,Input } from '@angular/core';
import {ServicesService} from '../services.service';
import {ActivatedRoute} from '@angular/router';
@Component({
  selector: 'app-privacy',
  templateUrl: './privacy.component.html',
  styleUrls: ['./privacy.component.css']
})
export class PrivacyComponent implements OnInit {
  @Input() lang;
  
  public privacy;
  constructor(private _servicesService:ServicesService, private route :ActivatedRoute) { }

  ngOnInit() {
   
    
    this._servicesService.listPrivacy().subscribe(
      (data:Response)=>{
        if(data['result']['code'] == 200){
          this.privacy = data['data'][0];
        }
      });
  }
  
}
