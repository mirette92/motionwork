import { Component, OnInit } from '@angular/core';
import {ServicesService} from '../services.service';
import {TranslateService} from '@ngx-translate/core';
import { ToastrService } from 'ngx-toastr';
import { MatProgressButtonOptions } from 'mat-progress-buttons';
@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.component.html',
  styleUrls: ['./contact-us.component.css']
})
export class ContactUsComponent implements OnInit {
 
  public objContactUs = {user_name:'',user_email:'',subject:'',message:''};
  public data;
  public contactLoading = false;

 

  constructor(private _servicesService:ServicesService,private translate: TranslateService, private toastr: ToastrService) {
  //  console.log(this.translate.stream('Send'))
   }
 
 btnopts: MatProgressButtonOptions = {
    active: false,
    text: 'Send',
    spinnerSize: 18,
    raised: true,
    stroked: false,
    buttonColor: 'primary',
    spinnerColor: 'primary',
    fullWidth: false,
    disabled: false,
    mode: 'indeterminate',
    
  }
  ngOnInit() {
    // this.translate.setDefaultLang('fr');
  }
onSubmit(){
  this.btnopts.active = true;
  this.contactLoading = true;
  return this._servicesService.contact_us(this.objContactUs).subscribe(
    (data:Response)=>{
      if(data['result']['code'] == 200){
        this.btnopts.active = false;
        this.contactLoading = false;
        this.toastr.success(this.translate.instant('Success'),this.translate.instant('Your mail is send successfully'));
      }else{
        this.btnopts.active = false;
        this.contactLoading = false;
        this.toastr.error(this.translate.instant('Failed'), this.translate.instant('Your mail is failed'));
      }
    }
    
    );}
}
