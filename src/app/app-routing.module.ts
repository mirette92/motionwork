import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HeaderComponent } from './header/header.component';
import { AboutComponent } from './about/about.component';
import { HomeComponent } from './home/home.component';

import { ServicesComponent } from './services/services.component';

import { CareerComponent } from './career/career.component';
import { TeamComponent } from './team/team.component';
import { CareerDetailsComponent } from './career/career-details/career-details.component'
import { PortfolioComponent } from './portfolio/portfolio.component';

import { FooterComponent } from './footer/footer.component';
import { ProjectDetailsComponent } from './project-details/project-details.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import {PrivacyComponent} from './privacy/privacy.component';
import {TermsComponent} from './terms/terms.component';

const routes: Routes = [
  {path:'',component:HomeComponent},
  // {path:'services',component:ServicesComponent},
  {path:'career',component:CareerComponent},
  {path:'team',component:TeamComponent},
  {path:'career/careerDetails/:id',component:CareerDetailsComponent},
  {path:'protofilo',component:PortfolioComponent},
  {path:'project/:id',component:ProjectDetailsComponent},
  {path:'contact_us',component:ContactUsComponent},
  {path:'privacy',component:PrivacyComponent},
  {path:'terms',component:TermsComponent}



  

];


@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash:true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const routingComponents = [HeaderComponent, 
  AboutComponent, 
  ServicesComponent, 
  PortfolioComponent,
  FooterComponent,
  ProjectDetailsComponent,
  ContactUsComponent,
  TermsComponent,
  PrivacyComponent

]

