import { Injectable } from '@angular/core';
import {HttpClient ,HttpHeaders} from '@angular/common/http';
import 'rxjs/add/operator/map';
import {environment} from '../environments/environment';


@Injectable({
  providedIn: 'root'
})
export class ServicesService {

  private _urlListService: string = environment.baseUrl+"listServices/";
  private _urlListBanner: string = environment.baseUrl+"listBanner/";
  private _urlListAbout: string = environment.baseUrl+"listAbout/";
  private _urlContactUs: string = environment.baseUrl+"contact_us";
  private _urlListSection: string = environment.baseUrl+"listSection/";
  private _urlGetProjectById: string = environment.baseUrl+"getProjectById/";
  private _urlListCareer: string = environment.baseUrl+"listCareer/";
  private _urlGetCareerById: string =  environment.baseUrl+"getCareerById/";
  private _urlListTeam:string = environment.baseUrl+"listEmployee/";
  private _urlListTerm:string = environment.baseUrl+"getTerm/";
  private _urlListPrivacy:string = environment.baseUrl+"getPrivacy/";

  constructor(private http: HttpClient) { }

  listTerm(){

    return this.http.get(this._urlListTerm+JSON.parse(sessionStorage.getItem('lang')));
  }
  listBanner(){

    return this.http.get(this._urlListBanner+JSON.parse(sessionStorage.getItem('lang')));
  }
  listAbout(){
    return this.http.get(this._urlListAbout+JSON.parse(sessionStorage.getItem('lang')));
  }
  listPrivacy(){

    return this.http.get(this._urlListPrivacy+JSON.parse(sessionStorage.getItem('lang')));
  }
  listService(){

    return this.http.get(this._urlListService+JSON.parse(sessionStorage.getItem('lang')));
  }
  listTeam(){

    return this.http.get(this._urlListTeam+JSON.parse(sessionStorage.getItem('lang')));
  }

  contact_us(Data){
    return this.http.post(this._urlContactUs,JSON.stringify(Data),
      {headers: new HttpHeaders({
        'Content-Type':  'application/json',
      
        
      })}
    ).map(data=>data);
  }


  listSection(){
    
    return this.http.get(this._urlListSection+JSON.parse(sessionStorage.getItem('lang')));
  }

  getProjectById(project_id){
  
    return this.http.get(this._urlGetProjectById+project_id+'/'+JSON.parse(sessionStorage.getItem('lang')));
  }

  listCareer(){

    return this.http.get(this._urlListCareer+JSON.parse(sessionStorage.getItem('lang')));
  }
  getCareerById(career_id){
  
    return this.http.get(this._urlGetCareerById+career_id+'/'+JSON.parse(sessionStorage.getItem('lang')));
  }


}
