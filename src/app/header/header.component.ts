import { Component, OnInit ,Input} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {ServicesService} from '../services.service';
import * as $ from 'jquery';
import 'bootstrap';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  @Input() arrBanner;
  constructor(private translate: TranslateService,private _servicesService:ServicesService) { }

  ngOnInit() {
    this._servicesService.listBanner().subscribe(
      (data:Response)=>{
          if(data['result']['code'] == 200){
            this.arrBanner = data['data'];
          }
      }
      );
  }

  ngAfterViewInit() {
    $('.carousel').carousel();
  }

}
