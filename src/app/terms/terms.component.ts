import { Component, OnInit ,Input } from '@angular/core';
import {ServicesService} from '../services.service';
import {ActivatedRoute} from '@angular/router';
@Component({
  selector: 'app-terms',
  templateUrl: './terms.component.html',
  styleUrls: ['./terms.component.css']
})
export class TermsComponent implements OnInit {
  @Input() lang;

  public term;
  constructor(private _servicesService:ServicesService, private route :ActivatedRoute) { }

  ngOnInit() {
    
    
    this._servicesService.listTerm().subscribe(
      (data:Response)=>{
        if(data['result']['code'] == 200){
          this.term = data['data'][0];
        }
      });
  }
  
}
